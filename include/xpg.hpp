// xpg - the xkcd password generator
// Class declarations
// author: klosure
// license: 3-Clause BSD License

#ifndef MY_XPG
#define MY_XPG

#include "main.hpp"
#include "sodium.h"

typedef unsigned int u_int;

class Xpg {

public:

  Xpg() = delete;
  Xpg(Xpg& x) = delete;
  ~Xpg();
  Xpg(Params p);

  std::vector<std::string> dict;
  std::string password = "";

  bool read_dict(const std::string &path);
  bool file_exists(const std::string& name) const;
  bool create_password();

private:

  Params m_par;

  std::string upper_first(std::string word) const;
};

#endif // MY_XPG
