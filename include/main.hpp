// xpg - the xkcd password generator
// Main ('Runner') header file
// author: klosure
// license: 3-Clause BSD License

#ifndef MY_MAIN
#define MY_MAIN

#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>
#include <random>
#include <fstream>
#include <climits>

#include "CLI11.hpp"

typedef unsigned int u_int;

struct Params {
  std::string casing_desired;
  u_int num_words;
  u_int num_pass;
  std::string to_insert;
  bool skip_first = false;
  std::string version;
  std::string dict_path = "inc/dict.txt";
};

// ---- Helper Functions ----

inline void lower(std::string &case_des) {
  std::for_each(case_des.begin(), case_des.end(), [](char& c) {
    c = ::tolower(c);
  });
}


inline bool check_casing(Params &p) {
  bool return_val = false;
  bool is_lower = true;
  try {
    lower(p.casing_desired);
  }
  catch(...){
    is_lower = false;
    std::cout << "can't convert to lowercase";
  }
  if (is_lower) {
    if (p.casing_desired == "pascal" || p.casing_desired == "camel") {
      if (p.casing_desired == "camel") {
	p.skip_first = true;
      }
      return_val = true;
    }
    else if (p.casing_desired == "snake" || p.casing_desired == "lisp") {
      if (p.casing_desired == "lisp") {
	p.to_insert = "-";
      }
      return_val = true;
    }
  }
  return return_val;
}


inline void print_version(const std::string &version) {
  std::cout << "xpg version: " << version << '\n';
}


inline bool check_args(Params& p, CLI::App& app) {
  bool return_val = false;
  if (app.count("-v")) {
    print_version(p.version);
    return return_val;
  }
  if (app.count("-w")) {
    if (p.num_words < 1 || p.num_words > 999) {
      throw "Bad user input";
    }
  }
  if (app.count("-n")) {
    if (p.num_pass < 1 || p.num_pass > UINT_MAX) {
      throw "Bad user input";
    }
  }
  if (app.count("-c")) {
    try {
      return_val = check_casing(p);
    }
    catch (...) {
      std::cout << "Bad user input";
    }
  }
  else if (!app.count("-c")) {
    return_val = true;
  }
  return return_val;
}

#endif //MY_MAIN
