// xpg - the xkcd password generator
// Xpg class implementation file
// author: klosure
// license: 3-Clause BSD License

#include "xpg.hpp"

// --- Constructors ---

Xpg::Xpg(Params p) : m_par(p) {}
Xpg::~Xpg() {}


// --- Member Functions ---

bool Xpg::read_dict(const std::string& path){
  std::ifstream infile(path);
  std::string line;
  while (infile >> line) {
    dict.push_back(line);
  }
 return true;
}


// fast way to check for file existence
bool Xpg::file_exists(const std::string &name) const {
  struct stat buffer;
  return (stat(name.c_str(), &buffer) == 0);
}


std::string Xpg::upper_first(std::string word) const {
  word.at(0) = toupper(word.at(0));
  return word;
}


bool Xpg::create_password() {
  std::ostringstream build_pass;
  while (this->m_par.num_pass) {
    u_int words = this->m_par.num_words;
    while (words) {
      uint32_t rnum = randombytes_uniform((uint32_t)dict.size());
      if (m_par.casing_desired == "snake" || m_par.casing_desired == "lisp") {
	--words;
	build_pass << dict.at(rnum);
	if (words) {
	  build_pass << m_par.to_insert;
	}
	continue;
      }
      else if (m_par.skip_first) {
	build_pass << dict.at(rnum);
	m_par.skip_first = false;
      }
      build_pass << upper_first(dict.at(rnum));
      --words;
    }
    std::cout << build_pass.str() << '\n';
    // reset the ostringstream
    build_pass.str("");
    build_pass.clear();
    build_pass.seekp(0);
    --m_par.num_pass;
  }
  return true;
}
