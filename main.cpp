// xpg - the xkcd password generator
// Main ('Runner') implementation file
// author: klosure
// license: 3-Clause BSD License

#include "xpg.hpp"

// --- Non-member functions ---

int main(int argc, char* argv[]) {
	
  if (sodium_init() < 0) {
    // panic! the library couldn't be initialized, it is not safe to use
    return 1;
  }

  const std::string VERSION = "0.1";
  Params p{ "pascal", 6, 1, "_", false, VERSION, (std::string(getenv("HOME"))+="/.xpg/dict.txt") };
  bool ran_ok = false;

  // parse the user input flags & options
  CLI::App app{ "Generates a xkcd 'CorrectHorseBatteryStaple' style password." };
  app.ignore_case();

  app.add_flag("-v,--version", "Display the version of xpg");

  app.add_option("-c,--case", p.casing_desired, "Casing styles: pascal, camel, snake, lisp");
  app.add_option("-w,--words", p.num_words, "Number of words to use - defaults to 4");
  app.add_option("-n,--numpass", p.num_pass, "Number of passwords to create - defaults to 1");
  app.add_option("-p,--path", p.dict_path, "Manually specify the path of a dictionary to use.");

  CLI11_PARSE(app, argc, argv);

  bool args_ok = false;
  try {
    args_ok = check_args(p, app);
  }
  catch(...){
    std::cout << "Problem parsing arguments! (Bad input values used?)";	
  }
  std::unique_ptr<Xpg> sp_xpg_obj;
  if (args_ok) {
    // good input, now construct object
    sp_xpg_obj = std::make_unique<Xpg>(p);
    // now try to read in the dictionary.
    bool read_in = false;
    try {
      if (sp_xpg_obj->file_exists(p.dict_path)) {
	read_in = sp_xpg_obj->read_dict(p.dict_path);
      }
      else {
	throw "File not found!";
      }
    }
    catch (...) {
      std::cout << "Exception trying to read dictionary file! (Does that file exist?)" << '\n';
      std::cout << "Put a dictionary at ~/.xpg/dict.txt" << '\n';
    }
    if (read_in) {
      // good read, now build password
      sp_xpg_obj->create_password();
      ran_ok = true;
    }
  }
  // exit, returning an int
  if (ran_ok) {
    return 0;
  }
  else {
    return 1;
  }
}
