![](logo.png)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

[Link to the original comic strip](https://xkcd.com/936/)

## Motivations:

Simply wanted a quick way to generate memorable passwords from the command line.

## Features:

+ choose your casing (camelCase, PascalCase, snake_case, lisp-case)
+ specify a specific number of words
+ generate any number of passwords
+ manual pages

## Prerequisites
+ Compiler capable of [C++14](https://en.cppreference.com/w/cpp/compiler_support) (*i.e. GCC5* or *Clang3.4*)
+ POSIX [make](https://pubs.opengroup.org/onlinepubs/009695399/utilities/make.html) (*GNU make*, *Bmake*, etc.)
+ [libsodium](https://github.com/jedisct1/libsodium)

## Install:

#### Source
```bash
# on ubuntu
sudo apt install build-essential libsodium-dev
git clone https://gitlab.com/klosure/xpg.git
cd xpg
make deps # creates ~/.xpg/dict.txt (the default dictionary)
make # build binary
make install # as root

# if you want to build a debug version
make debug

# if you want to build an optimized version
make opt
```

#### Binary
Check out the Releases page..

## Usage:

```bash
xpg                                # use default settings
xpg -c snake                       # use snake_case
xpg -c pascal                      # use PascalCase
xpg -c camel                       # use camelCase
xpg -c lisp                        # use lisp-case
xpg -w 12                          # specify 12 words to be used
xpg -n 6 -w 12                     # create 6 passwords each using 12 words
```

### Security considerations:

The default settings are meant to be sane and secure. None of the settings will degrade the security of your passwords, they simply cater to your personal preference.

The random number generator used is from libsodium, a new and strong crypto library. The RNG is also seeded by libsodium in the proper fashion. The reasoning behind choosing libsodium is that it is quickly installed via most unix package managers, therefore most people either already have it installed, or can [install it easily](https://libsodium.gitbook.io/doc/installation). I wanted to use only standard library features, but couldn't find a safe way to to so.

There has been much online debate regarding the safety of using this scheme. There is enough evidence to show that as long you are using passwords in a safe manner you will be safe using this scheme. This means using a password manager, and not using one password for all your different logins. You can read some of the arguments for and against the scheme [here](https://security.stackexchange.com/questions/62832/is-the-oft-cited-xkcd-scheme-no-longer-good-advice), [here](https://www.reddit.com/r/xkcd/comments/8vb9x3/is_password_strength_still_legit/), and [here](https://diogomonica.com/2014/10/11/password-security-why-the-horse-battery-staple-is-not-correct/). If you are looking for a good password manager, I suggest [pass](https://www.passwordstore.org/).

Feel free to send me suggestions for improving the security of this application. I'm open to being shown where there are errors.


## Built with:
Free and Open Source software and the help of the church of Emacs ;)

## Notes:
There are a number of alternate dictionaries provided. The initial one I used, although large, contained many hard to remember or pronounce medical words. I have recently replaced the default dictionary to be a more friendly one. These dictionaries can be found in *./include/dict/* Currently only ASCII english dictionaries are provided. I provide more than one dictionary to make things more difficult on a potential attacker. If you wish to change the dictionary, simply place one of these as *~/.xpg/dict.txt*

+ [eff-long.txt](https://www.eff.org/dice) - A list curated by the EFF, from the Reinhold list.
+ [eff-short.txt](https://www.eff.org/dice) - A list curated by the EFF, from the Reinhold list containing only short words.
+ [english-top40k.txt](https://gist.github.com/h3xx/1976236) - Top common english words, cleaned up to remove french, german, and spanish words.
+ [english-479k.txt](https://github.com/dwyl/english-words) - A very large list containing many rare and uncommon words.
+ [us-cities.txt](http://jordonmeyer.com/text-list-of-us-cities/) - A list of U.S. city names
+ [us-names.txt](https://github.com/smashew/NameDatabases/tree/master/NamesDatabases) - A list of common names in the U.S.
+ all-combined.txt - The default dictionary, combines all of these. (minus english-479k)

## License / Disclaimer:
BSD 3-Clause; See LICENSE.md.

Please use a secure password manager.
