CXX=c++
RM=rm -f
PREFIX=/usr/local
CXXFLAGS=--std=c++14 -pedantic -Wall -Wextra
LDFLAGS = -I include
LDFLAGS += -I tests
LDFLAGS += -I $(PREFIX)/include
LDLIBS=-L$(PREFIX)/lib -lsodium

BINDIR=$(PREFIX)/bin
MANDIR=$(PREFIX)/share/man/man1

.POSIX: all debug opt

all: xpg

debug: CXXFLAGS += -O0 -ggdb -save-temps -fsanitize=undefined -fsanitize=address -fno-omit-frame-pointer -fno-sanitize-recover
debug: LDFLAGS += -lasan -lubsan
debug: clean xpg

opt: CXXFLAGS += --pipe -O2 -fpie -march=native -fstack-protector-strong
opt: LDFLAGS += -pie -Wl,-z,relro -Wl,-z,now
opt: clean xpg

xpg: xpg.o main.o
	$(CXX) $(LDFLAGS) -o xpg xpg.o main.o $(LDLIBS)

xpg.o: xpg.cpp
	$(CXX) $(LDFLAGS) $(CXXFLAGS) -c xpg.cpp

main.o: main.cpp
	$(CXX) $(LDFLAGS) $(CXXFLAGS) -c main.cpp

clean:
	$(RM) *.o *.s *.ii

distclean: clean
	$(RM) *.key *.tar.gz *~ xpg test_xpg

#test: test.cpp
#> run tests

dist: distclean
	tar -czvf ../xpg-source.tar.gz ./
	mv ../xpg-source.tar.gz ./

deps:
	mkdir ~/.xpg
	cp ./include/dict/all-combined.txt ~/.xpg/dict.txt

install:
	cp ./xpg $(BINDIR)
	cp ./xpg.1 $(MANDIR)

uninstall:
	rm $(BINDIR)/xpg
	rm $(MANDIR)/xpg.1
	rm -r ~/.xpg
